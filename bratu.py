from GaussQuad import *
from ops import *
import matplotlib.pyplot as plt

def forceFunc(lamda, x):
    return -( np.pi**2 ) * np.sin( np.pi * x ) + lamda * np.exp( np.sin( np.pi * x ) )

def analyticSol(x):
    return np.sin( np.pi * x )

def approxSol(U, element, xi):
    ans = 0
    for i in [1,2]:
        ans += linearBF( i, xi ) * U[ element + i - 2 ]
    return ans

def delApproxSol(U, element, h, xi):
    ans = 0
    for i in [1,2]:
        ans += (2/h) * delLinearBF( i, xi ) * U[ element + i - 2 ]
    return ans

def g(A):
    if A[0] == 1 and A[1] == 1:
        return g11
    
    if A[0] == 1 and A[1] == 2 or A[0] == 2 and A[1] == 1:
        return g12
    
    if A[0] == 2 and A[1] == 2:
        return g22

def g11(U, lamda, element, h, xi):
    term1   = (2/h) * delLinearBF( 1, xi ) * delLinearBF( 1, xi )
    term2   = (lamda * h/2) * np.exp( approxSol( U, element, xi ) ) * linearBF( 1, xi ) * linearBF( 1, xi )
    ans     = term1 - term2
    return np.array([ans]);

def g12(U, lamda, element, h, xi):
    term1   = (2/h) * delLinearBF( 1, xi ) * delLinearBF( 2, xi )
    term2   = (lamda * h/2) * np.exp( approxSol( U, element, xi ) ) * linearBF( 1, xi ) * linearBF( 2, xi )
    ans     = term1 - term2
    return np.array([ans]);

def g22(U, lamda, element, h, xi):
    term1   = (2/h) * delLinearBF( 2, xi ) * delLinearBF( 2, xi )
    term2   = (lamda * h/2) * np.exp( approxSol( U, element, xi ) ) * linearBF( 2, xi ) * linearBF( 2, xi )
    ans     = term1 - term2
    return np.array([ans]);
    
def h1(U, lamda, element, h, X, xi):
    term1   = delApproxSol(U, element, h, xi) * delLinearBF(1, xi)
    term2   = (lamda * h/2) * linearBF(1, xi) * np.exp(approxSol(U, element, xi))
    term3   = (h/2) * linearBF(1, xi) * forceFunc( lamda, xOfXi( X, element, xi ) )
    ans     = -term1 + term2 - term3
    return ans

def h2(U, lamda, element, h, X, xi):
    term1   = delApproxSol(U, element, h, xi) * delLinearBF( 2, xi )
    term2   = (lamda * h/2) * linearBF( 2, xi ) * np.exp( approxSol( U, element, xi ) )
    term3   = (h/2) * linearBF( 2, xi ) * forceFunc( lamda, xOfXi( X, element, xi ) )
    ans     = -term1 + term2 - term3
    return ans

def H(A):
    if A == 1:
        return h1
    if A == 2: 
        return h2
    
def FEM_Bratu(N, h, X, n, lamda, U0, t, gqpoint, tol):
    np.set_printoptions(precision = 2)
    # A           = np.zeros((N+1,N+1))
    exactSol    = np.zeros(N+1)
    for i in range(N+1):
        exactSol[i] = analyticSol(i*h)

    np.random.seed(7)
    U_lastStep  = np.random.rand(N+1)
    delU        = np.ones(N+1)

    while np.linalg.norm(delU) > tol:
        A = np.zeros((N+1,N+1))

        for element in range(1,N+1):

            A_local = np.zeros((n,n))
            
            for i in range(1,n+1):
                for j in range(1,n+1):
                    gij                 = g([i,j])                  
                    gij_partial         = functools.partial(gij, U_lastStep, lamda, element, h)
                    A_local[i-1][j-1]   = gaussQuad(gij_partial, gqpoint, -1, 1)    
                    
            A[ element - 1 : element - 1 + n, element - 1 : element - 1 + n ] += A_local

        b = np.zeros(N+1)

        for element in range(1, N+1):
            
            b_local = np.zeros((n))
            
            for i in range(1,n+1):
                hi              = H(i)
                hi_partial      = functools.partial(hi, U_lastStep, lamda, element, h, X)
                b_local[i-1]    = gaussQuad(hi_partial, gqpoint, -1, 1)

            b[ element - 1 : element - 1 + n ] += b_local

        ############ Applying BC #################
        #### Dirichlet BC ####
        A[0,0]  = 1
        A[0,1]  = 0
        b[0]    = U0

        #### Neumann BC ####
        b[-1]   += t
        ##########################################

        delU        = np.matmul(np.linalg.inv(A), b)
        U_lastStep  = U_lastStep + delU
        print("abs delU:    ", np.linalg.norm(delU))
    
    error = np.linalg.norm(U_lastStep-exactSol)*(h**(0.5))

    # print("A: \n",A)
    # print("B: \n",b)

    # print("Approx Sol:  ", U_lastStep,"\n")
    # print("Exact Sol:   ", exactSol,"\n")
    # print("Error:       ", error,"\n")

    xaxs = np.arange(0,1,1/(N+1))
    plt.plot(xaxs, exactSol, 'r--', xaxs, U_lastStep, 'bs')
    # plt.show()
    return error


""" def I11(lamda, h, U, element, xi):
    ans = (lamda*h/2)*np.exp(approxSol(U, element, xi))
    for i in [1,1]:
        ans = ans*linearBF(i, xi)
    return np.array([ans])

def I12(lamda, h, U, element, xi):
    ans = (lamda*h/2)*np.exp(approxSol(U, element, xi))
    for i in [1,2]:
        ans = ans*linearBF(i, xi)
    return np.array([ans])

def I21(lamda, h, U, element, xi):
    ans = (lamda*h/2)*np.exp(approxSol(U, element, xi))
    for i in [2,1]:
        ans = ans*linearBF(i, xi)
    return np.array([ans])

def I22(lamda, h, U, element, xi):
    ans = (lamda*h/2)*np.exp(approxSol(U, element, xi))
    for i in [2,2]:
        ans = ans*linearBF(i, xi)
    return np.array([ans])

def I(A):
    if A[0] == 1 and A[1] == 1:
        return I11
    
    if A[0] == 1 and A[1] == 2:
        return I12
    
    if A[0] == 2 and A[1] == 1:
        return I21
    
    if A[0] == 2 and A[1] == 2:
        return I22 """

""" def k1(U, element, h, xi):
    return delLinearBF(1, xi)*delApproxSol(U, element, xi, h)

def k2(U, element, h, xi):
    return delLinearBF(2, xi)*delApproxSol(U, element, xi, h)

def K(A):
    if A == 1:
        return k1
    if A == 2: 
        return k2
"""

""" 
def g21(U, lamda, element, h, xi):
    term1 = (2/h)
    for i in [2,1]:
        term1 = term1*delLinearBF(i, xi)
    
    term2 = (lamda*h/2)*np.exp(approxSol(U, element, xi))
    for i in [2,1]:
        term2 = term2*linearBF(i, xi)

    ans = term1+term2
    return np.array([ans]); """