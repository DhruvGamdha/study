import numpy as np
import functools
import matplotlib.pyplot as plt
from GaussQuad import *

def elementDomain(N, domain, h):
    X = A = np.zeros((N+1,2))
    for element in range(1,N+1):
        X[element][0] = domain[0] + (element-1)*h
        X[element][1] = domain[0] + (element)*h
    return X

## Defining the Linear Basis Function in Xi domain (-1, 1)
def linearBF(a, xi):
    if a == 1: 
        return (1-xi)/2
    elif a==2:
        return (1+xi)/2

## Defining the derivative of Basis Function in Xi domain (-1,1)
def delLinearBF(a,xi):
    if a == 1:
        return (-1/2)
    elif a == 2:
        return (1/2)

## Defining isoparametric Mapping from Xi to X
def xOfXi(X, element, xi):
    return X[element][0]*linearBF(1,xi) + X[element][1]*linearBF(2,xi) 

