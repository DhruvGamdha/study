import numpy as np

def gaussQuad(func, npoints, a, b):
    
    if npoints == 1:
        x = np.array([0])
        w = np.array([2])
    
    elif npoints == 2: 
        x = np.array([1/np.sqrt(3),-1/np.sqrt(3)])
        w = np.array([1,1])
    
    elif npoints == 3:
        x_val = np.sqrt(3/5)
        x = np.array([-x_val, 0,x_val])
        
        w_val1 = 8/9
        w_val2 = 5/9
        w = np.array([w_val2,w_val1,w_val2])
        
    elif npoints == 4:
        x_val1 = np.sqrt(3/7+(2/7)*(np.sqrt(6/5)))
        x_val2 = np.sqrt(3/7-(2/7)*(np.sqrt(6/5)))
        x = np.array([-x_val1,-x_val2, x_val2, x_val1])
        
        w_val1 = (18-np.sqrt(30))/36
        w_val2 = (18+np.sqrt(30))/36
        w = np.array([w_val1, w_val2, w_val2, w_val1])
    
    elif npoints == 5: 
        x_val1 = (1/3)*np.sqrt(5+2*np.sqrt(10/7))
        x_val2 = (1/3)*np.sqrt(5-2*np.sqrt(10/7))
        x = np.array([-x_val1, -x_val2, 0, x_val2, x_val1])
        
        w_val1 = (322-13*np.sqrt(70))/900
        w_val2 = (322+13*np.sqrt(70))/900
        w = np.array([w_val1, w_val2, 128/225, w_val2, w_val1])
    
    y = ((b-a)/2)*x+(a+b)/2
#     print("w: ", w)
#     print("y: ", y)
#     print("func: ", func)
#     print("func(y): ", func(y))
    return ((b-a)/2)*np.matmul(w, func(y))