from bratu import *

tol = 10**-3

## Parameters
## BC ##
U0 = 0  ## Dirichlet ##
t = -np.pi  ## Neumann ##
lamda = 2
## Degree of basis function
# d = 1

## Nodes per element
n = 2 

## Domain
domain = [0,1]

# GuassQuad points
gqpoint = 1

## Number of divisions of the domain
N_list = [2**5-1, 2**6-1, 2**7-1, 2**8-1, 2**9-1]
errors = []
h_list = []

for N in N_list:
    # Element length
    h = (domain[1]-domain[0])/N
#     print("h:", h)
    h_list.append(h)
    
    X = elementDomain(N, domain, h)
    error = FEM_Bratu(N, h, X, n, lamda, U0, t, gqpoint, tol)
    errors.append(error)
    print("Error:   ",error)

print("-------")

# i = 0
# d = 4
# print("Slope:   ",(np.log(errors[i+d])-np.log(errors[i]))/(np.log(h_list[i+d])-np.log(h_list[i])))